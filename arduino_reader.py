import os
import serial


class IRReader:
	def __init__(self):
		self.serial = serial.Serial(os.environ['ARDUINO_PORT'], int(os.environ['ARDUINO_BAUD']), timeout=1)
		self.values = []

	def read(self):
		line = self.serial.readline().strip()
		#print(line)
		if len(line) > 0:
			if line[0] == '[' and line[-1] == ']':
				self.values = [int(x) for x in line[1:-1].split(',')]
			else:
				print('read bad line ' + line)
		else:
			print('read empty line')

	def get(self, i):
		return self.values[i]