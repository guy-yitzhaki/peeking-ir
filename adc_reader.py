import sys
import time

import RPi.GPIO as GPIO


class ADC:
    PIN_CLK = 18
    PIN_DO = 27
    PIN_DI = 22
    PIN_CS = 17

    def __init__(self):
        GPIO.setmode(GPIO.BCM)

        # change these as desired - they're the pins connected from the
        # SPI port on the ADC to the Cobbler


        # set up the SPI interface pins
        GPIO.setup(self.PIN_DI, GPIO.OUT)
        GPIO.setup(self.PIN_DO, GPIO.IN)
        GPIO.setup(self.PIN_CLK, GPIO.OUT)
        GPIO.setup(self.PIN_CS, GPIO.OUT)


    # read SPI data from ADC8032
    def read(self, channel):
        # 1. CS LOW.
        GPIO.output(self.PIN_CS, True)  # clear last transmission
        GPIO.output(self.PIN_CS, False)  # bring CS low

        # 2. Start clock
        GPIO.output(self.PIN_CLK, False)  # start clock low

        # 3. Input MUX address
        for i in [1, 1, channel]:  # start bit + mux assignment
            if i == 1:
                GPIO.output(self.PIN_DI, True)
            else:
                GPIO.output(self.PIN_DI, False)

        GPIO.output(self.PIN_CLK, True)
        GPIO.output(self.PIN_CLK, False)


        # 4. read 8 ADC bits
        ad = 0
        for i in range(8):
            GPIO.output(self.PIN_CLK, True)
            GPIO.output(self.PIN_CLK, False)
            ad <<= 1  # shift bit
            if GPIO.input(self.PIN_DO):
                ad |= 0x1  # set first bit

        # 5. reset
        GPIO.output(self.PIN_CS, True)

        return ad


if __name__ == "__main__":
    adc = ADC()
    channel = int(sys.argv[1]) if len(sys.argv) > 1 else 0
    print('reading channel {0}'.format(channel))
    try:
        while True:
            print("ADC[{0}]: {1}".format(channel, adc.read(channel)))
            time.sleep(0.1)
    except KeyboardInterrupt:
        GPIO.cleanup()
