import os
import sys
import time

import requests

import RPi.GPIO as GPIO
import network_detector as network
from adc_reader import ADC


class Sensor:
    def __init__(self, idx, server, port, trigger_distance, stop_trigger_distance):
        self.triggered = False
        self.idx = idx
        self.play_url = 'http://{0}:{1}/play/{2}'.format(server, port, self.idx+1)
        self.pause_url = 'http://{0}:{1}/pause/{2}'.format(server, port, self.idx + 1)
        self.trigger_distance = trigger_distance
        self.stop_trigger_distance = stop_trigger_distance

    def check_value(self, value):
        if self.triggered:
            if value > self.stop_trigger_distance:
                self.get_url(self.pause_url)
                print('sensor {0} paused'.format(self.idx + 1))
                self.triggered = False
        else:
            if value < self.trigger_distance:
                self.get_url(self.play_url)
                print('sensor {0} triggered'.format(self.idx + 1))
                self.triggered = True

    def get_url(self, url):
        try:
            requests.get(url, timeout=0.1)
        except requests.exceptions.ConnectionError:
            print('could not get url {0}'.format(url))


class Controller:
    simulate = os.environ['SIMULATE_ADC'] == 'True'
    server_id = os.environ['CONTROLLER_SOUND_SERVER']
    port = 5000
    num_sensors = int(os.environ['NUM_SENSORS'])
    subnetwork = os.environ['PEEKING_NETWORK']
    loop_delay = float(os.environ['LOOP_DELAY'])
    channels = [int(x) for x in os.environ['ADC_CHANNELS'].split(',')]
    trigger_distances = [int(x) for x in os.environ['TRIGGER_DISTANCES'].split(',')]
    stop_trigger_distances = [int(x) for x in os.environ['STOP_TRIGGER_DISTANCES'].split(',')]
    sensors = []

    def __init__(self):
        print('controller starting')
        print('-- simulate: {0}'.format(self.simulate))
        print('-- sensors: {0}'.format(self.num_sensors))
        print('-- server id: {0}'.format(self.server_id))
        print('-- subnetwork : {0}'.format(self.subnetwork))
        print('-- channels: {0}'.format(self.channels))
        print('-- trigger : {0}'.format(self.trigger_distances))
        print('-- stop_trigger : {0}'.format(self.stop_trigger_distances))
        if len(self.channels) < self.num_sensors:
            raise RuntimeError('missing adc channel')
        if len(self.trigger_distances) < self.num_sensors:
            raise RuntimeError('missing trigger distance')
        if len(self.stop_trigger_distances) < self.num_sensors:
            raise RuntimeError('missing stop trigger distance')
        self.adc = ADC()
        servers = self.get_server_map()
        self.server = servers[self.server_id]
        for i in range(self.num_sensors):
            self.sensors.append(
                Sensor(i, self.server, self.port, self.trigger_distances[i], self.stop_trigger_distances[i]))
        self.stop_all()

    def stop_all(self):
        requests.get('http://{0}:{1}/stopall'.format(self.server, self.port))

    def get_server_map(self):
        while True:
            server_map = network.get_servers(self.subnetwork)
            if len(server_map) == 0:
                print('no servers found, waiting...')
                time.sleep(1)
            else:
                break
        print(server_map)
        return server_map

    def loop(self):
        print('starting main event loop')
        counter = 0
        while True:
            for i in range(self.num_sensors):
                if not self.simulate:
                    val = self.adc.read(self.channels[i])
                else:
                    val = counter
                    counter += 1
                    counter = counter % 50
                if val != 0:
                    self.sensors[i].check_value(val)
            time.sleep(self.loop_delay)

    def teardown(self):
        self.stop_all()
        GPIO.cleanup()


try:
    controller = Controller()
except KeyboardInterrupt:
    print("stopped by user, before loop")
    GPIO.cleanup()
    sys.exit()	

try:	
    controller.loop()
except KeyboardInterrupt:
    print("stopped by user")
    controller.teardown()
