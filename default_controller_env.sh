# 1 "controller_env.sh"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "controller_env.sh"
export SIMULATE_ADC=True
export CONTROLLER_SOUND_SERVER=1
export NUM_SENSORS=4
export PEEKING_NETWORK=10.100.102.0
export LOOP_DELAY=0.2
export ADC_CHANNELS=1,2,3,4
export TRIGGER_DISTANCES=15,15,15,15
export STOP_TRIGGER_DISTANCES=60,60,60,60
