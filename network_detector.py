import sys

import requests

from nmap import nmap

debug = False


def get_live_ips(network):
    print('scanning')
    nm = nmap.PortScanner()
    nm.scan(hosts=network+'/24', arguments='-sn')
    hosts_list = [(x, nm[x]['status']['state']) for x in nm.all_hosts()]
    print('found {0} hosts'.format(len(hosts_list)))
    return hosts_list


def get_server_number(host):
    try:
        url = 'http://{0}:5000/id'.format(host)
        if debug:
            print('trying '+url)
        r = requests.get(url, timeout=0.1)
        if r.status_code == 200:
            if debug:
                print('found server')
            server_num = r.text
            return server_num
    except requests.exceptions.ConnectionError:
        if debug:
            print('not a server')
    except:
        if debug:
            print(sys.exc_info()[0])
    return -1


def get_servers(network):
    hosts = get_live_ips(network)
    servers = {}
    for host, status in hosts:
        server_num = get_server_number(host)
        if server_num != -1:
            servers[server_num] = host
    return servers


if __name__ == "__main__":
    server_map = get_servers('192.168.0.0')
    print(server_map)
