import serial
import ConfigParser as configparser


class Relay:
    def __init__(self):
        config = read_config('peeking.config')
        self.serial = serial.Serial(config['port'], config['baud'])

	def on(self, idx):
		print('relay on %d' % idx)
		self.serial_command(str(idx + 1))

	def off(self, idx):
		print('relay off %d' % idx)
		self.serial_command(str(4 + idx + 1))

	def serial_command(self, cmd):
		self.serial.write(cmd)

	def teardown(self):
		pass