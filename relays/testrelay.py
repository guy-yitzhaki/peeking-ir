import os
from cmd import Cmd

import RPi.GPIO as GPIO

pinList = [int(x) for x in os.environ['RELAY_PINS']]


class RelayPrompt(Cmd):
    def do_on(self, args):
        if len(args) == 0:
            pin = 0
        else:
            pin = int(args.split(' ')[0])
        GPIO.output(pin, GPIO.LOW)

    def do_off(self, args):
        if len(args) == 0:
            pin = 0
        else:
            pin = int(args.split(' ')[0])
        GPIO.output(pin, GPIO.HIGH)

    def do_quit(self, args):
        """Quits the program."""
        print('quitting')
        GPIO.cleanup()
        raise SystemExit


if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    for i in pinList:
        GPIO.setup(i, GPIO.OUT)
        GPIO.output(i, GPIO.HIGH)
    try:
        prompt = RelayPrompt()
        prompt.prompt = '> '
        prompt.cmdloop('starting...')
    except KeyboardInterrupt:
        GPIO.cleanup()
