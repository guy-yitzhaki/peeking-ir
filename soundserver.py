import os

import pygame as pg
from flask import Flask

from relays import gpio_relay as relay

app = Flask(__name__)

server_id = os.environ['SOUND_SERVER_ID']
sound_file_names = os.environ['SOUND_FILES']
sound_files = sound_file_names.split(',')
channels = []

print('server starting')
print('-- id: {0}'.format(server_id))
print('-- sound files: {0}'.format(sound_files))

relay = relay.Relay()
pg.mixer.init(channels=len(sound_files))
sounds = [pg.mixer.Sound('sounds/'+name) for name in sound_files]
channels = [sound.play(-1) for sound in sounds]
for c in channels:
    c.pause()
#map(lambda c: c.pause(), channels)
playing = [False for x in channels]
print('ready!')


@app.route('/id')
def id():
    return server_id


@app.route('/stopall')
def stop_all():
    for n in range(len(channels)):
        stop_channel(n)
    return server_id


@app.route('/play/<num>')
def play(num):
    print('playing {0}'.format(num))
    n = int(num) - 1
    playing[n] = True
    channels[n].unpause()
    relay.on(n)
    return 'play {0}'.format(num)


@app.route('/pause/<num>')
def pause(num):
    print('stopping {0}'.format(num))
    n = int(num) - 1
    stop_channel(n)
    return 'pause {0}'.format(num)


def stop_channel(n):
    playing[n] = False
    channels[n].pause()
    relay.off(n)


@app.route('/teardown/')
def teardown():
    relay.teardown()

